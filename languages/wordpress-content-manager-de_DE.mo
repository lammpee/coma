��    [      �      �      �     �     �  �        �     �     �  	   �     �  	   �     �  	   �  #   �  !        0     7     <     I     P     \     b     n  	   z     �     �     �     �     �     �     �       
        $     5     K     W     ^     p     �     �     �     �     �     �     �     �  K   	     O	     U	     \	     n	     �	  	   �	  
   �	     �	     �	     �	     �	     �	     �	  
   �	     
     
     
     
     4
     F
  "   X
     {
     �
     �
     �
     �
     �
     �
     �
     �
     �
  K   �
     '     .     7     C  
   J     U  !   d     �  
   �     �     �     �  �  �     �     �  �  �  
   �     �     �  	   �     �     �     �     	  E     .   [     �     �     �     �     �     �     �     �  	             .     E     Q     d     �     �  #   �     �     �     �     �               )  *   =  +   h  )   �     �  
   �     �  	   �     �  W        Z     a     h     ~     �     �  
   �     �     �     �     �               '     A     G     \  #   c  !   �     �  >   �     �  	     &        4     =     N  	   c     m     s  	   z  X   �  
   �     �     �     
            *   1     \     n     }     �     �   %PLUGIN_PATH%: Plugin-Path %THEME_PATH%: Theme-Path <code>Warning: Experimentally, not tested</code> <br />Enables Pages/Post Revisions-Tool from WordPress, for areas and components. Activate Activate Area? Activate Component? Activated Add Component Add Field Administrator All Roles Allowed deleting Pages/Posts Cache. Allowed deleting all Controllers. Append Area Area delete? Author Author mode Cache Cache Reset Cache-Path: Component Component Select Content-Manager Roles Contributor Controller-Browser Controllers have been deleted. Copy Current Window/Tab DEFAULT: Wordpress-Root Deactivate Deactivate Area? Deactivate Component? Deactivated Delete Delete Component? Delete Controllers Delete Pages/Posts Cache Deleted Pages/Posts Cache Deleting need to be accepted. Disable Edit Edit Component Editor Editor-Mode Enables Pages/Post Revisions-Tool from WordPress, for areas and components. Error Filter Global Properties Here, something went wrong ... Image Quality? Is cached Link-Title Max. %d component(s) Mode? Move Move Component? New Window/Tab No post Not cached Page Page Properties Pages Pages-Cache activate? Pages/Posts Cache Parent Window/Tab Path to save cached pages as file. Post Posts Posts-Cache activate? Preview Preview mode Refresh Thumbnails Remove Role Roles Save Save pages/posts html-markup by first load as flat file for other requests. Select Settings Show Posts: Static Subscriber Top Window/Tab Use Post-Revision from Wordpress? WP Post-Revision? Warranties content-manager properties delete? wordpress-content-manager Project-Id-Version: Content-Manager-1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-04-27 13:09+0100
PO-Revision-Date: 2015-09-10 23:03+0200
Language-Team: Thorn-Welf Walli <wp-cm@lammpee.com>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
X-Poedit-Keywordslist: __;_e
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.4
Last-Translator: 
X-Poedit-Searchpath-0: ..
 %PLUGIN_PATH%: Plugin-Pfad %THEME_PATH%: Theme-Pfad <code>Warnung: Experimentel, nicht getestet.</code> <br />Aktiviert die Seiten/Beitrag Revisions-Funktion von Wordpress, für Bereiche und Komponenten. <br>Wiederherstellt beim zurücksetzen auf eine ältere Revision, alle zu diesem Zeitpunkt vorhandenen Bereiche und Komponenten. <br /><code>Löscht alle aktuellen.</code> <br /><code>Hinweis: Es werden keine Revisionen beim Erstellen/Bearbeiten einer Komponente erzeugt.</code> Aktivieren Bereiech aktivieren? Komponente aktivieren? Aktiviert Komponente hinzufügen Feld hinzufügen Administrator Alle Rollen Das löschen der gesamten Seiten/Beiträge Zwischenspeicher erlauben. Das löschen der gesamten Controller erlauben. Hinzufügen Bereich Bereiech löschen? Autor Autoren-Ansicht Zwischenspeicher Zwischenspeicher zurücksetzen Zwischenspeicher-Pfad: Komponent Komponente auswählen Content-Manager Rollen Mitarbeiter Controller-Browser Controller wurden gelöscht. Kopieren Aktuelles Fenster/Tab DEFAULT: Wordpress-Hauptverzeichnis Deaktivieren Bereiech deaktvieren? Komponente deaktvieren? Deaktiviert Löschen Komponente löschen? Controller löschen Seiten/Beiträge Zwischenspeicher löschen Seiten/Beträge Zwischenspeicher gelöscht. Das Löschen muss erst akzeptiert werden. Deaktiviert Bearbeiten Komponente bearbeiten Redakteur Editor-Modus Aktiviert das Seiten/Beitrag Revision-Tool von Wordpress für Bereiche und Komponenten. Fehler Filter Globale Eigenschaften Hier ging etwas schief... Bild Qualität? Zwischengespeichert Link-Titel Max. %d Komponente(n) Modus? Verschieben Komponente verschieben? Neues Fenster/Tab Kein Beitrag Nicht zwischengespeichert Seite Seiten Eigenschaften Seiten Seiten-Zwischenspeicher aktivieren? Seiten/Beiträge Zwischenspeicher Eltern Fenster/Tab Pfad zum speichern der zwischengespeicherten Seiten als Datei. Beitrag Beiträge Beiträge-Zwischenspeicher aktivieren? Vorschau Vorschau-Ansicht Bilder aktualisieren Entfernen Rolle Rollen Speichern Speichert von Seiten/Beiträgen das HTMl-Markup ab, und gibt dieses beim aufruf zurück. Auswählen Einstellungen Beiträge anzeigen: Statisch Abonnent Oberstes Fenster/Tab Beitrags-Revision von Wordpress verwenden? WP Post-Revision? Berechtigungen Content-Manager Eigenschaften löschen? Content-Manager 