# Content-Manager Plugin for Wordpress
Content-Manager ist ein Plugin für Worpress, dieses ermöglicht den Einsatz von Bereichen und deren Komponenten. Damit wird ein Baukasten Prinzip möglich.

1. [Vorbereitung](#markdown-header-1-vorbereitung)
2. [WP-Admin](#markdown-header-2-admin)
    -   [Einstellungen](#markdown-header-einstellungen)
    -   [Cache](#markdown-header-a-cache)
    -   [Rollen](#markdown-header-rollen)
    -   [Controller-Browser](#markdown-header-controller-browser)
3. [Klassen / Statische Helper-Klassen](#markdown-header-3-klassen-Statische-helper-klassen)
    -  [Helper](#markdown-header-helper)
    -  [Klassen](#markdown-header-klassen)

## 1. Vorbereitung
Für die Einführung in das Plugin steht ein Beispiel Wordpress-Theme zur verfügung.

[Wordpress-Content-Manager-Theme](https://bitbucket.org/lammpee/wordpress-content-manager-theme)

### Installation

Um mit Content-Manager zu arbeiten, muss das Plugin ersteinmal installiert werden.
Dafür muss der Inhalt unter dem angegebenen Pfad abgelegt werden.
> wp-content/plugins/wordpress-content-manager

Zum schluss muss das Plugin unter Wordpress aktiviert werden.

> **Hinweis zur Benutzung mit Themes.**
> Die Verwendung von Content-Manager ist nur in einem Theme möglich, dass für diese angepasst wurde.

Für die verwendung Themes muss beachtet werden:

Es setzt diese Verzeichnisstruktur vorraus:

| Ordner                     | Beschreibung                                                 |
| -------------------------- | ------------------------------------------------------------ |
| `contentmanager`           | Hauptverzeichnis von Content-Manager im Theme.               |                
| `contentmanager/area`                   | Verzeichnis für die Bereichs-Klassen.                        |        
| `contentmanager/component`              | Verzeichnis für die Komponenten-Klassen.                     |        
| `contentmanager/template`               | Verzeichnis für die Templates.                               |
| `contentmanager/template/area`          | Verzeichnis für die Templates der Bereiche.                  |            
| `contentmanager/template/component`     | Verzeichnis für die Templates der Komponenten.               |    
Es ist zu empfehlen, das Beispiel-Theme zu verwenden.            

## 2. WP-Admin
### Einstellungen
Hier befinden sich weitere Aktionen und Eigenschaften.

-   **Seiten/Beiträge Cache**

    Es ist möglich von Seiten und Beiträgen, statische Markup kopien zu erstellen.
    Dafür muss unter Einstellung > Content-Manager der jeweilge Cache aktiviert werden.

    Wenn diese Option aktiv ist, wird beim ersten aufruf einer Seite, eine statische Kopie des Markups angelegt.
    Dies geschieht nur bei einem nicht angemeldeten Benutzer.
    Bei änderrungen kann des Inhalts einer Seite, kann unter den Seiten-Einstellungen die Option "Reset Cache" unter dem Tab "Cache" aktiviert werden. 
    Wenn "Reset Cache" aktiv wird beim aufruf der Seite eine neue Kopie angelegt.
    
    > Pfad: **%PLUGIN_DIRECTORY%/cache**

-   **Controller löschen**
    Mit dieser Aktion, werden alle Controller (Bereich/Komponenten) gelöscht.
    
### Cache
Hier können die Thumbnails neugeneriert werden.

### Rollen
Hier befindet sich eine Verwaltung der Wordpress Benutzer-Rollen und deren Berechtigungen.
Dazu bringt Content-Managerr auch eigene Berechtigungen mit.

### Controller-Browser
Im Controller-Browser können die Eigenschaften einer Komponenten bearbeitet oder gelöscht werden.
    
## 3. Klassen / Statische Klassen

### Helper
#### Template
Alle angegebenen Funktionen und Eigenschaften sind statisch.

> Klasse: **\ContentManager\Helper\Template**

| Function name                     | Description                                                               
| --------------------------------- | ------------------------------------------------------------------------- 
| `POST()`                          | Gibt POST-Daten mit ungeprefixten Argument zurück.                        
| `GET()`                           | Gibt GET-Daten mit ungeprefixten Argument zurück.                         
| `getAreasByPage()`                | Ruft Bereiche unter der angegebenen Seiten-Id ab.                         
| `getArea()`                       | Ruft den Bereich ab, der an der angegebenen Position und Klasse liegt.Wenn static aktiv, wird die Seiten-Id ignoriert und der Bereich ist überall vorhanden, wo dieser mit Position und Klasse angegeben ist. |
| `isEditMode()`                    | Ruft ab ob der Benutzer sich im Autoren-Modus befindet.                   
| `isPreviewMode()`                 | Ruft ab ob der Benutzer sich im Vorschau-Modus befindet.                  
| `hasRights()`                     | Ruft ab ob der Benutzer Bereichtigungen hat, um die Seite zu bearbeiten.  
| `renderTagAttributes()`           | Rendert Tag-Attribute mit angegebenen Prefix.                             
| `getPageId()`                     | Ruft die aktuelle Seite-Id ab.                                            
| `getGlobalProperty()`             | Ruft die angegebene Globale-Eigenschaft ab.                               
| `getPageProperty()`               | Ruft die angegebene Seiten-Eigenschaft ab.                               
| `setPageProperty()`               | Legt die angegebene Seiten-Eigenschaft fest.                                
| `getProperty()`                   | Ruft die angegebene Seiten-Eigenschaft ab, wenn diese leer, Globale-Eigenschaft.
| `getParentId()`                   | Ruft die aktuelle Id des Eltern Controllers zurück.                        

#### Component
Alle angegebenen Funktionen und Eigenschaften sind statisch.

> Klasse: **\ContentManager\Helper\Component**

| Function name                    | Description                                                  |
| -------------------------------- | ------------------------------------------------------------ |
| `getAreaByPositionAndParent()`   | Ruft das angegebene Area ab.                                 |
| `getAreaIdByPositionAndParent()` | Ruft die Id des angegebenen Area ab.                         |
| `getAreaByPosition()`            | Ruft das angegebene Area ab.                                 |
| `getAreaById()`                  | Ruft das angegebene Area ab.                                 |
| `areaExistByPosition()`          | Überprüft ob das mit der Position angegebene Area existiert. |
| `areaExistById()`                | Überprüft ob das mit der Id angegebene Area existiert.       |
| `getComponentById()`             | Ruft den angegebenen Controller ab.                          |

#### Controller
Alle angegebenen Funktionen und Eigenschaften sind statisch.

> Klasse: **\ContentManager\Helper\Controller**

| Function name                    | Description                                                  |
| -------------------------------- | ------------------------------------------------------------ |------------------------------------- |
| `getControllersByPageId()`       | Ruft die der Seite zugewiesenen Controller aus der Datenbank ab.                                    |
| `getControllersByParentId()`     | Ruft die einer zugewiesenen Controller, unter Controller aus der Datenbank ab.                      |
| `getControllerById()`            | Ruft einen Controller aus der Datenbank ab.                                                         |
| `parseController()`              | Verarbeitet die aus der Datenbank abgefragten Daten und liefert ein Objekt zurück.                  |
| `getPageIdFromController()`      | Ruft die Seiten-Id des Controllers ab. Dies geschieht auch über darüber liegende Controller hinaus. |
| `getControllerChildrens()`       | Ruft untergeordnete Controller von einem Controller ab.                                             |
| `deleteAllControllers()`         | Löscht alle Controller.                                                                             |
| `getChildrenCount()`             | Ruft die Anzahl der untergeordneten Controller ab.                                                  |
| `removeController()`             | Löscht den angegebenen Controller mit seinen unter Controllern.                                     |

#### Base
Alle angegebenen Funktionen und Eigenschaften sind statisch.
> Klasse: **\ContentManager\Helper\Base**

| Function name                    | Description                                               |
| -------------------------------- | --------------------------------------------------------- |
| `ROLE_CAPS`                      | Content-Manager Berechtigungen.                           |
| `getCurrentUserRole()`           | Ruft die Rolle des Benutzers ab.                          |
| `getEditableRoles()`             | Ruft alle Berechtigungen ab.                              |
| `getAllCapabilities()`           | Ruft alle Berechtigungen ab.                              |
| `roleHasCap()`                   | Ruft ab, ob der Benutzer die angegebene Berechtigung hat. |
| `isAdministrator()`              | Ruft ab, ob der Benutezr ein Administrator ist.           |
| `getGlobalProperties()`          | Ruft die Globalen-Eigenschaften ab.                       |
| `getPageProperties()`            | Ruft die Seiten-Eigenschaften ab.                         |
| `log()`                          | Fügt ein Log-Eintrag hinzu.                               |
| `getLogs()`                      | Ruft alle Log-Einträge ab.                                |
| `renderAdminNotice()`            | Rendert WP-Admin-Notice.                                  |
| `cleanUploadDirectory()`         | Löscht alle generierten Dateien im Upload-Ordner.         |
| `getAttachments()`               | Ruft alle Attachments mit dem angebenen Typen ab.         |
| `performContent()`               | Bereitet Text-Inhalt aus dem Editor zum rendern vor.      |
| `performClassName()`             | Bereitet einen Klassnnamen zu.                            |
| `getWPOption()`                  | Gibt ein dem Plugin zugeordnete Option zuück.             |
| `setWPOption()`                  | Legt eine dem Plugin zugeordnete Option fest.             |
| `getWPOptionPrefixed()`          | Ruft den geprefixed Optionsnamen ab.                      |
| `getPrefixedName()`              | Ruft den geprefixed Namen ab.                             |

### Klassen

#### PropertyDialog
> Klasse: **\ContentManager\PropertyDialog**

| Function name                    | Description                                               |
| -------------------------------- | --------------------------------------------------------- |
| `addTab()`                       | Erzeugt ein neues Tab.                                    |
| `addFields()`                    | Für die angegebenen Felder hinzu.                         |
| `render()`                       | Rendert den Dialog mit den angebenen Eigenschaften.       |
| `getTab()`                       | Ruft das angegeben Tab an                                 |
| `uniqid()`                       | Ruft ein uniq Id ab, mit oder ohne Prefix.                |
| `getName()`                      | Ruft den Namen ab.                                        |
| `name()`                         | Legt den Namen fest.                                      |
| `getTitle()`                     | Ruft den Titel ab.                                        |
| `title()`                        | Legt den Titel fest.                                      |
| `getAllFieldNames()`             | Ruft alle Feldnamen ab.                                   |

#### PropertyDialogTab
> Klasse: **\ContentManager\PropertyDialog\Tab**

| Function name                    | Description                                               |
| -------------------------------- | --------------------------------------------------------- |
| `render()`                       | Rendert den Tab mit den angebenen Eigenschaften.
| `addFields()`                    | Fügt Felder hinzu. 
| `createField()`                  | Erzeugt ein Feld.
| `addMediaImageSelectField()`     | Fügt ein Mediathek Feld für Bilder hinzu.
| `addMediaSelectField()`          | Fügt ein Mediathek Feld hinzu.
| `addTextField()`                 | Fügt ein Textfeld hinzu. 
| `addTextArea()`                  | Fügt ein mehrzeiliges Textfeld hinzu.
| `addEditor()`                    | Fügt ein Editor hinzu.
| `addDropDown()`                  | Fügt ein DropDown hinzu.
| `addCategorySelect()`            | Fügt ein Kategorieauswahl-DropDown hinzu.
| `addCheckBox()`                  | Fügt eine Checkbox hinzu.
| `addRadioBox()`                  | Fügt eine Radiobox hinzu.
| `addLink()`                      | Fügt einen Link hinzu.
| `addCheckBox()`                  | Fügt eine Checkbox hinzu.
| `addPageSelect()`                | Fügt ein Seitenauswahl-DropDown hinzu.
| `getFields()`                    | Gibt alle Felder vom Tab zurück.
| `getName()`                      | Ruft den Namen ab.
| `name()`                         | Legt den Namen fest.
| `getTitle()`                     | Ruft den Titel ab.
| `title()`                        | Legt den Titel fest. 
| `getFieldNames()`                | Ruft alle Feldnamen vom Tab ab.

#### PropertyDialogField
> Klasse: **\ContentManager\PropertyDialog\Field**

| Function name                    | Description                                               |
| -------------------------------- | --------------------------------------------------------- |
| `render()`                       | Rendert das Feld mit den angebenen Eigenschaften.
| `getTag()`                       | Ruft den Tag ab.
| `getValue()`                     | Ruft den Wert ab.
| `getName()`                      | Ruft den Namen ab.
| `getTitle()`                     | Ruft den Titel ab.
| `getDescription()`               | Ruft die Beschreibung ab.
| `getId()`                        | Ruft die Id ab.
| `getMediaType()`                 | Ruft den Medien-Typ ab.
| `hasLabel()`                     | Ruft ab ob ein Label vorhanden ist.
| `tag()`                          | Legt den Tag fest.
| `id()`                           | Legt die Id fest.
| `type()`                         | Legt den Typ fest.
| `name()`                         | Legt den Namen fest.
| `title()`                        | Legt den Titel fest.
| `value()`                        | Legt den Wert fest.
| `rows()`                         | Legt den Zeilenanzahl fest.
| `size()`                         | Legt die sichtbaren Zeichenanzahl fest.
| `mediaType()`                    | Legt den Media-Typ fest.
| `categorySelect()`               | Legt fest, dass das Feld eine Kategorieauswahl ist.
| `checked()`                      | Legt fest ob die Checkbox ausgewählt ist.
| `selected()`                     | Legt fest ob das Feld ausgewählt ist.
| `disabled()`                     | Legt fest ob das Feld deaktiviert ist.
| `readonly()`                     | Legt fest ob das Feld nur Lesbar ist.
| `options()`                      | Legt die Optionen fest.
| `defaultValue()`                 | Legt den Default-Wert fest.
| `description()`                  | Legt die Beschreibung fest.
| `items()`                        | Legt die Items fest.

#### PropertyDialogButton
> Klasse: **\ContentManager\PropertyDialog\Button**

| Function name                    | Description                                               |
| -------------------------------- | --------------------------------------------------------- |
| `render()`                       | Rendert das Feld mit den angebenen Eigenschaften.
