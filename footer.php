<?php

if (\CoMa\Helper\Base::isEditMode() && !is_admin() || get_current_screen()->parent_base == 'coma') {

    ?>

    <script type="text/html" id="coma-template-log">

        <div class="{{= type }} notice is-dismissible">
            <p>{{= text }}</p>
        </div>

    </script>

    <div class="coma-controller partial" data-coma-controller="components/Logs" data-partial="wp-content-manage/logs">

        <div>

            <?php

            foreach (\CoMa\Helper\Base::getLogs() as $log) {

                CoMa\Helper\Base::renderAdminNotice($log['text'], $log['type']);

            }

            ?>

        </div>

    </div>

    <script type="text/javascript">

        var coma = {
            require: {
            baseUrl : '<?php echo (\CoMa\MIN_JS)? \CoMa\PLUGIN_URL.'js/main.js' : \CoMa\PLUGIN_URL.'src/js/main.js'; ?>',
                config: {
                    paths: {}
                }
            }
        };
    </script>

        <?php

        if (\CoMa\MIN_JS) {

        ?><script src="<?php echo \CoMa\PLUGIN_URL.'js/main.js' ; ?>" async defer></script><?php

        } else {
        ?>

            <script type="text/javascript">
                coma.require.config.paths['codemirror'] = '../../js/codemirror';
                coma.require.config.paths['libs'] = '../../js/libs';
            </script>
            <script data-main="<?php echo \CoMa\PLUGIN_URL; ?>src/js/config" src="<?php echo \CoMa\PLUGIN_URL; ?>js/require.js" async defer></script><?php

        }

    wp_enqueue_media();
    echo '<div class="coma-controller" data-coma-controller="CoMa"' . CoMa\Helper\Base::renderTagAttributes(array('deep-modal' => 'coma-dialog', 'page-id' => CoMa\Helper\Base::getSession('page-id'), 'ajax' => \CoMa\ADMIN_URL), 'data') . '></div>';
    if (\CoMa\Helper\Base::isEditMode()) {
        include(\CoMa\PLUGIN_TEMPLATE_PATH . 'modal.php');
    }


}

?>