## Tutorial

1.  [Komponente](#markdown-header-1-komponente)
    -   [Erstellen einer eigenen Komponente](#markdown-header-erstellen-einer-eigenen-komponente)
    -   [Komponente rendern](#markdown-header-komponente-rendern)
2.  [Bereich](#markdown-header-1-bereich)
    -   [Erstellen eines eigenen Bereichs](#markdown-header-erstellen-eines-eigenen-bereichs)
    -   [Bereich rendern](#markdown-header-bereich-rendern)

## 1. Komponente

### Erstellen einer eigenen Komponente
Eine Komponente besteht aus zwei Dateien, diese liegen an verschiedenen Verzeichnissen ab.

In deinem Template-Verzeichnis befindet sich das Verzeichnis "contentmanager", in diesem ist das Verzeichnis "Component", dort kommt die Klasse deiner Komponente rein.
> Beispiel: contentmanager/component/Example.php

Passend zur Klasse muss es auch noch ein Template zur Komponente geben, dieses muss unter dem Verzeichnia "template/component" angelegt werden.
> Beispiel: contentmanager/template/component/example.php

#### Klasse

Die Klasse der Komponente ist wie folgt aufgebaut:

##### Eigenschaften

| Name                                    | Beschreibung                             |
| --------------------------------------- | ---------------------------------------- |
| `$TEMPLATE_NAME`                        | Sichtbarer Name der Komponente.          |
| `$TEMPLATE_ID`                          | Interne Komponenten-Id der Komponente.   |
| `$TEMPLATE_PATH`                        | Relativer Pfad zum Komponenten-Template. |

##### Funktionen

| Name                                    | Beschreibung                              |
| --------------------------------------- | ----------------------------------------- |
| `getPropertyDialog()`                   | Gibt die Eigenschaften der Komponente an. | 

    <?php
    
    class Example extends ContentManager_ThemeComponent
    {
    
        /**
         * @var string
         */
        public static $TEMPLATE_NAME = 'Example Component';
        /**
         * @var string
         */
        public static $TEMPLATE_ID = 'example';
        /**
         * @var string
         */
        public static $TEMPLATE_PATH = 'example';
    
        /**
         * @return PropertyDialog
         */
        public function getPropertyDialog()
        {
            $propertyDialog = parent::defaultPropertyDialog();
            $tab = $propertyDialog->getTab();
            // CheckBox
            $tab->addCheckBox('show_post_content', 'Show Page/Post Content')->description('Ignore headline and copy property and show content from Page / Post. ');
            // Image
            $tab->addMediaImageSelectField('imageId', 'Image');
            // TextBox
            $tab->addEditor('copy', 'Copy');
            return $propertyDialog;
        }
    
    }
    
    ?>
    
#### Template

Die Eigenschaften der Komponente werden über `$this->getProperties()` abgefragt.

    <?php
    
    /**
     * @type Example $this
     */
    $properties = $this->getProperties();
    
    if ($properties['show_page_content']) {
        global $post;
        setup_postdata($post);
        $headline = get_the_title();
        $copy = ContentManager_BaseHelper::performContent($post->post_content);
    } else {
        $headline = $properties['headline'];
        $copy = ContentManager_BaseHelper::performContent($properties['imageId']);
    }
    
    ?>
    
    <article>
    
        <?php
    
        if ($properties['imageId']) {
            if (ContentManager_TemplateHelper::isEditMode()) {
                echo '<p>Image:' . wp_get_attachment_thumb_url($properties['imageId']) . '</p>';
            } else {
                ?>
                <div class="img">
                    <img src="<?php echo wp_get_attachment_thumb_url($properties['imageId']); ?>"
                         title="<?php echo $headline; ?>"/>
                </div>
                <?php
            }
        }
    
        ?>
    
        <header>
            <?php
    
            if ($properties['primary_headline']) {
                if ($properties['headline']) echo '<h1>' . $properties['headline'] . '</h1>';
            } else {
                if ($properties['headline']) echo '<h2>' . $properties['headline'] . '</h2>';
            }
    
            ?>
        </header>
    
        <?php
    
        echo $copy;
    
        ?>
        
    </article>

### Komponente rendern
Eine Komponente kann auch ganz aus dem Plugin-Context heraus gerendert werden.
Dafür muss nur eine Instanz erzeugt werden. Über die Funktion `mapProperties()` können Eigenschaften zugeordnet werden.

> Hinweis: Beim rendern muss die Option `edit` auf `false` gesetzt werden, dies blendet die Autoren-Oberläche aus.
>> $exampleComponent->render(array('edit' => false))

> **Tip:** Ruft alle Name der Komponente-Eigenschaften ab.
>> `$exampleComponent->getPropertyDialog()->getAllFieldNames()`

    <?php
    
        $exampleComponent = new Example();
        $exampleComponent->mapProperties(array(
            'headline' => 'static headline'
        ));
        $exampleComponent->render(array('edit' => false));
    
    ?>

## 2. Bereich

### Erstellen eines eigenen Bereichs
Ein Bereich besteht aus zwei Dateien, diese liegen an verschiedenen Verzeichnissen ab.

In deinem Template-Verzeichnis befindet sich das Verzeichnis "contentmanager", in diesem ist das Verzeichnis "Area", dort kommt die Klasse deines Bereichs rein.
> Beispiel: contentmanager/area/ExampleArea.php

Passend zur Klasse muss es auch noch ein Template zum Bereich geben, dieses muss unter dem Verzeichnia "template/area" angelegt werden.
> Beispiel: contentmanager/template/area/exampleArea.php

#### Klasse

Die Klasse des Bereichs ist wie folgt aufgebaut:

##### Eigenschaften

| Name                                    | Beschreibung                             |
| --------------------------------------- | ---------------------------------------- |
| `$TEMPLATE_NAME`                        | Sichtbarer Name der Komponente.          |
| `$TEMPLATE_ID`                          | Interne Komponenten-Id der Komponente.   |
| `$TEMPLATE_PATH`                        | Relativer Pfad zum Komponenten-Template. |

##### Funktionen

| Name                                    | Beschreibung                              |
| --------------------------------------- | ----------------------------------------- |
| `getClasses()`                          | Gibt alle Klassen (Komponenten) zurück, die im Bereich angelegt werden können. | 
| 

    <?php
    
    class ExampleArea extends ContentManager_ThemeArea
    {
    
        /**
         * @var string
         */
        public static $TEMPLATE_NAME = 'Example Area';
        /**
         * @var string
         */
        public static $TEMPLATE_ID = 'exampleArea';
        /**
         * @var string
         */
        public static $TEMPLATE_PATH = 'exampleArea';
    
        public function __construct()
        {
            $this->setClass(get_class($this));
        }
    
        /**
         * @return array
         */
        public static function getClasses()
        {
            return array(Example);
        }
    
    }
    
    ?>
    
#### Template

Die Komponenten des Bereichs werden über `$this->getChildrens()` abgefragt.

    <?php
    
    /**
     * @type ExampleArea $this
     */
    
    foreach($this->getChildrens() as $component) {
        $component->render();
    }
    
    ?>
    
    
### Bereich rendern
Ein Bereich wird über die TemplateHelper Funktion `getArea` gerendert.

##### Argumente
| Name        | Beschreibung
| ----------- | -------------
| `string` `$name`     | Legt den Namen des Bereiches fest.
| `class` `$class`    | Legt die Klasse des Bereiches fest.
| `bool` `$static`     | Gibt an ob Bereich statisch ist.


    <?php   
    
        ContentManager_TemplateHelper::getArea($name, $class, $static);
    
    ?>